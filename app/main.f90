program cradle
    implicit none

    character(len=26), parameter :: LOWER = &
        'abcdefghijklmnopqrstuvwxyz'
    character(len=26), parameter :: UPPER = &
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    character(len=1), parameter :: TAB = char(9)
    character(len=*), parameter :: NEWLINE = new_line('a')

    character(len=1) :: look ! lookahead character

    call init()
    call assignment()
    if (look /= NEWLINE) call expected('Newline')
contains
    subroutine get_char()
        !! Read new character from input stream
        integer :: stat
        character(len=256) :: msg

        read(*,'(A1)', advance='no', iostat=stat, iomsg=msg) look
        if (is_iostat_eor(stat)) then
            look = NEWLINE
        else if (is_iostat_end(stat)) then
            look = char(0)
        else if (stat /= 0) then
            error stop msg
        end if
    end subroutine

    subroutine error(s)
        !! Report an error
        character(len=*), intent(in) :: s

        write(*, *)
        write(*, '(A)') "Error: " // s // "."
    end subroutine

    subroutine abort(s)
        !! Report error and halt
        character(len=*), intent(in) :: s

        call error(s)
        error stop
    end subroutine

    subroutine expected(s)
        !! Report what was expected
        character(len=*), intent(in) :: s

        call abort(s // " Expected")
    end subroutine

    subroutine match(x)
        !! Match a specific input character
        character(len=1), intent(in) :: x

        if (look == x) then
            call get_char()
        else
            call expected('"' // x // '"')
        end if
    end subroutine

    function is_alpha(c)
        !! Recognize an alpha character
        character(len=1), intent(in) :: c
        logical :: is_alpha

        character(len=52), parameter :: ALPHABET = &
                LOWER // UPPER

        is_alpha = index(ALPHABET, c) > 0
    end function

    function is_digit(c)
        !! Recognize a decimal digit
        character(len=1), intent(in) :: c
        logical :: is_digit

        character(len=10), parameter :: DIGITS = '0123456789'

        is_digit = index(DIGITS, c) > 0
    end function

    function is_addop(c)
        !! Recognize an add operation
        character(len=1), intent(in) :: c
        logical :: is_addop

        is_addop = index('+-', c) > 0
    end function

    function get_name()
        !! Get an identifier
        character(len=1) :: get_name

        if (.not. is_alpha(look)) call expected("Name")
        get_name = upcase(look)
        call get_char()
    end function

    function get_num()
        !! Get a number
        character(len=1) :: get_num

        if (.not. is_digit(look)) call expected("Integer")
        get_num = look
        call get_char()
    end function

    subroutine emit(s)
        !! Output a string with tab
        character(len=*), intent(in) :: s

        write(*, '(A)', advance='no') TAB // s
    end subroutine

    subroutine emit_ln(s)
        !! Output a string with tab and newline
        character(len=*), intent(in) :: s

        call emit(s)
        write(*, *)
    end subroutine

    subroutine init()
        !! Initialize
        call get_char()
    end subroutine

    function upcase(c)
        !! Return the uppercase of a letter
        character(len=1), intent(in) :: c
        character(len=1) :: upcase

        integer :: position

        position = index(LOWER, c)
        if (position > 0) then
            upcase = UPPER(position:position)
        else
            upcase = c
        end if
    end function

    subroutine ident()
        !! Parse and translate an identifier
        character(len=1) :: name

        name = get_name()
        if (look == '(') then
            call match('(')
            call match(')')
            call emit_ln('BSR ' // name)
        else
            call emit_ln('MOVE ' // name // '(PC),D0')
        end if
    end subroutine

    subroutine factor()
        !! Parse and translate a math factor
        if (look == '(') then
            call match('(')
            call expression()
            call match(')')
        else if (is_alpha(look)) then
            call ident()
        else
            call emit_ln("MOVE #" // get_num() // ',D0')
        end if
    end subroutine

    subroutine multiply()
        !! Recognize and translate a multiply
        call match('*')
        call factor()
        call emit_ln("MULS (SP)+,D0")
    end subroutine

    subroutine divide()
        !! Recognize and translate a divide
        call match('/')
        call factor()
        call emit_ln('MOVE (SP)+,D1')
        call emit_ln('DIVS D1,D0')
    end subroutine

    subroutine term()
        !! Parse and translate a math term
        call factor()
        do while (index('*/', look) > 0)
            call emit_ln("MOVE D0,-(SP)")
            select case (look)
            case ("*")
                call multiply()
            case ("/")
                call divide()
            end select
        end do
    end subroutine

    subroutine add()
        !! Recognize and translate an add
        call match('+')
        call term()
        call emit_ln("ADD (SP)+,D0")
    end subroutine

    subroutine subtract()
        !! Recognize and translate an add
        call match('-')
        call term()
        call emit_ln("SUB (SP)+,D0")
        call emit_ln("NEG D0")
    end subroutine

    subroutine expression()
        !! Parse and translate a math expression
        if (is_addop(look)) then
            call emit_ln('CLR D0')
        else
            call term()
        end if
        do while (is_addop(look))
            call emit_ln("MOVE D0,-(SP)")
            select case (look)
            case ("+")
                call add()
            case ("-")
                call subtract()
            end select
        end do
    end subroutine

    subroutine assignment()
        !! Parse and translate an assignment statement
        character(len=1) :: name

        name = get_name()
        call match('=')
        call expression()
        call emit_ln("LEA " // name // "(PC),A0")
        call emit_ln("MOVE D0,(A0)")
    end subroutine
end program
